# Paper KeyForge Tokens

![A full set of paper KeyForge tokens](doc/all_tokens.jpg)

* [Letter (8.5 x 11") tokens PDF](pdf/keyforge_tokens_letter.pdf)
* [A4 tokens PDF](pdf/keyforge_tokens_a4.pdf)

This project is for a full set of paper tokens for [KeyForge](https://www.fantasyflightgames.com/en/products/keyforge/). It makes three keys, a chain tracker card and token, fifteen Æmber tokens, eleven one-damage tokens, five three-damage tokens, five power tokens, and five stun tokens from one sheet of paper.

## Instructions

* [Letter (8.5 x 11") instructions](doc/instructions_letter.pdf)
* [A4 instructions](doc/instructions_a4.pdf)

Print the tokens PDF. If you want to colour your tokens, it's probably easiest to colour them before you cut them out.

### Æmber, damage, power, stun

It's easiest to fold an entire strip of tokens at a time. Cut out a strip of tokens.

![A strip of tokens, cut apart from the other strips but not from each other.](doc/small_tokens-cut_strip.png)

Fold the sides as marked.

![A strip of tokens with the sides folded.](doc/small_tokens-folded_strip.png)

Then, cut the tokens apart.

![Individual tokens that have been cut from a strip.](doc/small_tokens-cut_apart.png)

### Keys 

Cut the keys out and crease the folds as marked. Then, push the two slits together with the ends of the paper inside the token.

![The ends of the key end up inside it.](doc/key.png)

### Chain tracker token

Fold the long sides back first.

![Chain tracker token with the sides folded back.](doc/chain_token-sides_folded_back.png)

Fold the corners where marked. They should all be right angles.

![Chain tracker token folded into a square sort of thing.](doc/chain_token-corners_folded.png)

Tuck one end into the other.

![Chain tracker token with one end tucked into the other so it stays together.](doc/chain_token-end_tucked_in.png)

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.